FROM node:10.15.0-alpine as develop

LABEL MAINTAINER="HARCHHI harc.chan@maxwin.com.tw"

COPY ./ /var/maxwin

WORKDIR /var/maxwin

CMD ["echo", "dev"]

FROM node:10.15.0-alpine as production

LABEL MAINTAINER="HARCHHI harc.chan@maxwin.com.tw"

COPY ./ /var/maxwin

WORKDIR /var/maxwin

CMD ["echo", "prod"]
