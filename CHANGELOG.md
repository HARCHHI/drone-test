## [1.9.4](https://bitbucket.org/HARCHHI/drone-test/compare/1.9.3...1.9.4) (2021-08-27)


### Bug Fixes

* fix ([4c2fc4e](https://bitbucket.org/HARCHHI/drone-test/commits/4c2fc4ea404916e5a87cd4100912a29b0e69d716))

## [1.9.3](https://bitbucket.org/HARCHHI/drone-test/compare/1.9.2...1.9.3) (2021-08-27)


### Bug Fixes

* fix ([735cfb9](https://bitbucket.org/HARCHHI/drone-test/commits/735cfb9730bf0bc615239fab9b8a1e589c85008a))
* release changelog ([7ce465b](https://bitbucket.org/HARCHHI/drone-test/commits/7ce465b3da0b8b2ce168bbb0882f50ca5f6e6b84))

## [1.9.1](https://bitbucket.org/HARCHHI/drone-test/compare/1.9.0...1.9.1) (2021-08-27)


### Bug Fixes

* fix ([96f3781](https://bitbucket.org/HARCHHI/drone-test/commits/96f378136b01468a269058098c61730517713b97))
* ifx ([854fa4c](https://bitbucket.org/HARCHHI/drone-test/commits/854fa4c2650a4190cc180956b01767fe50c46a10))

## [1.9.1-beta.1](https://bitbucket.org/HARCHHI/drone-test/compare/1.9.0...1.9.1-beta.1) (2021-08-27)


### Bug Fixes

* fix ([96f3781](https://bitbucket.org/HARCHHI/drone-test/commits/96f378136b01468a269058098c61730517713b97))
* ifx ([854fa4c](https://bitbucket.org/HARCHHI/drone-test/commits/854fa4c2650a4190cc180956b01767fe50c46a10))

# [1.9.0](https://bitbucket.org/HARCHHI/drone-test/compare/1.8.2...1.9.0) (2021-08-27)


### Bug Fixes

* fix ([70c6a53](https://bitbucket.org/HARCHHI/drone-test/commits/70c6a53a1ff67dbf03ab473ea4965e5f7530dedc))


### Features

* feat ([ff6f11f](https://bitbucket.org/HARCHHI/drone-test/commits/ff6f11f8285c3b4dccba2271b3d8b5920742b5ff))

## [1.8.2](https://bitbucket.org/HARCHHI/drone-test/compare/1.8.1...1.8.2) (2021-08-27)


### Bug Fixes

<<<<<<< HEAD
* fix ([70c6a53](https://bitbucket.org/HARCHHI/drone-test/commits/70c6a53a1ff67dbf03ab473ea4965e5f7530dedc))
=======
>>>>>>> master
* fix something ([f833f74](https://bitbucket.org/HARCHHI/drone-test/commits/f833f7487645002c74be994c839ba6506f95a6f9))

## [1.8.1](https://bitbucket.org/HARCHHI/drone-test/compare/1.8.0...1.8.1) (2021-08-26)


### Bug Fixes

* drone ([3548de3](https://bitbucket.org/HARCHHI/drone-test/commits/3548de30df8ddde2839c18a6044b211dd46f07d8))

## [1.8.1-beta.1](https://bitbucket.org/HARCHHI/drone-test/compare/1.8.0...1.8.1-beta.1) (2021-08-26)


### Bug Fixes

* drone ([3548de3](https://bitbucket.org/HARCHHI/drone-test/commits/3548de30df8ddde2839c18a6044b211dd46f07d8))

# [1.8.0](https://bitbucket.org/HARCHHI/drone-test/compare/1.7.0...1.8.0) (2021-08-26)


### Bug Fixes

* drone ([69e8812](https://bitbucket.org/HARCHHI/drone-test/commits/69e881209d2af3e528f744b23497a34aa4d181a4))
* drone ([ac608a9](https://bitbucket.org/HARCHHI/drone-test/commits/ac608a95b97a51142197dcee2fbe7a3038afb5c0))
* fix ([c9dcfa6](https://bitbucket.org/HARCHHI/drone-test/commits/c9dcfa652cabe902fc66cb91bf9c3c7056a12ee5))
* 修正drone ([59b8592](https://bitbucket.org/HARCHHI/drone-test/commits/59b85929099194fd2345509b04a2ad7690cdf097))
* 加入debug ([6c242e5](https://bitbucket.org/HARCHHI/drone-test/commits/6c242e5063feaa7004a39a8c92007cd3e503a26b))
* 嘗試修正semantic release ([92024d9](https://bitbucket.org/HARCHHI/drone-test/commits/92024d90eb11aafe02055bbdf4dcecfb337c9c18))
* 嘗試升版 ([4f35e3d](https://bitbucket.org/HARCHHI/drone-test/commits/4f35e3d33a776f18cffb800d523f79a0b85c396d))
* 調整branch name ([97caa0c](https://bitbucket.org/HARCHHI/drone-test/commits/97caa0c7f43cd6054ae4b105ebf3c3dc608e2554))
* 調整channel ([4a7186b](https://bitbucket.org/HARCHHI/drone-test/commits/4a7186bb74aaafe987bdbf6d5b095da07a846fb2))
* **drone:** 調整branch condition ([f5a4b5d](https://bitbucket.org/HARCHHI/drone-test/commits/f5a4b5dfd6313ef0d177488ec31e28a98f0209f7))
* 降版嘗試 ([53d00d9](https://bitbucket.org/HARCHHI/drone-test/commits/53d00d9d3a93b3e62989c24145a2df06fc686767))


### Features

* feat ([c540af8](https://bitbucket.org/HARCHHI/drone-test/commits/c540af8fa21aca849c8740542088126e0bcd6913))

# [1.8.0-beta.2](https://bitbucket.org/HARCHHI/drone-test/compare/1.8.0-beta.1...1.8.0-beta.2) (2021-08-26)


### Bug Fixes

* 修正drone ([59b8592](https://bitbucket.org/HARCHHI/drone-test/commits/59b85929099194fd2345509b04a2ad7690cdf097))

# [1.8.0-beta.1](https://bitbucket.org/HARCHHI/drone-test/compare/1.7.1-beta.4...1.8.0-beta.1) (2021-08-26)


### Features

* feat ([c540af8](https://bitbucket.org/HARCHHI/drone-test/commits/c540af8fa21aca849c8740542088126e0bcd6913))

## [1.7.1-beta.4](https://bitbucket.org/HARCHHI/drone-test/compare/1.7.1-beta.3...1.7.1-beta.4) (2021-08-26)


### Bug Fixes

* drone ([69e8812](https://bitbucket.org/HARCHHI/drone-test/commits/69e881209d2af3e528f744b23497a34aa4d181a4))
* 嘗試修正semantic release ([92024d9](https://bitbucket.org/HARCHHI/drone-test/commits/92024d90eb11aafe02055bbdf4dcecfb337c9c18))

## [1.7.1-beta.3](https://bitbucket.org/HARCHHI/drone-test/compare/1.7.1-beta.2...1.7.1-beta.3) (2021-08-26)


### Bug Fixes

* drone ([ac608a9](https://bitbucket.org/HARCHHI/drone-test/commits/ac608a95b97a51142197dcee2fbe7a3038afb5c0))

## [1.7.1-beta.2](https://bitbucket.org/HARCHHI/drone-test/compare/1.7.1-beta.1...1.7.1-beta.2) (2021-08-26)


### Bug Fixes

* 加入debug ([6c242e5](https://bitbucket.org/HARCHHI/drone-test/commits/6c242e5063feaa7004a39a8c92007cd3e503a26b))

## [1.7.1-beta.1](https://bitbucket.org/HARCHHI/drone-test/compare/1.7.0...1.7.1-beta.1) (2021-08-26)


### Bug Fixes

* fix ([c9dcfa6](https://bitbucket.org/HARCHHI/drone-test/commits/c9dcfa652cabe902fc66cb91bf9c3c7056a12ee5))
* 嘗試升版 ([4f35e3d](https://bitbucket.org/HARCHHI/drone-test/commits/4f35e3d33a776f18cffb800d523f79a0b85c396d))
* 調整branch name ([97caa0c](https://bitbucket.org/HARCHHI/drone-test/commits/97caa0c7f43cd6054ae4b105ebf3c3dc608e2554))
* 調整channel ([4a7186b](https://bitbucket.org/HARCHHI/drone-test/commits/4a7186bb74aaafe987bdbf6d5b095da07a846fb2))
* **drone:** 調整branch condition ([f5a4b5d](https://bitbucket.org/HARCHHI/drone-test/commits/f5a4b5dfd6313ef0d177488ec31e28a98f0209f7))
* 降版嘗試 ([53d00d9](https://bitbucket.org/HARCHHI/drone-test/commits/53d00d9d3a93b3e62989c24145a2df06fc686767))

## [1.7.1-beta.1](https://bitbucket.org/HARCHHI/drone-test/compare/1.7.0...1.7.1-beta.1) (2021-08-26)


### Bug Fixes

* 嘗試升版 ([4f35e3d](https://bitbucket.org/HARCHHI/drone-test/commits/4f35e3d33a776f18cffb800d523f79a0b85c396d))
* 調整branch name ([97caa0c](https://bitbucket.org/HARCHHI/drone-test/commits/97caa0c7f43cd6054ae4b105ebf3c3dc608e2554))
* **drone:** 調整branch condition ([f5a4b5d](https://bitbucket.org/HARCHHI/drone-test/commits/f5a4b5dfd6313ef0d177488ec31e28a98f0209f7))
* 降版嘗試 ([53d00d9](https://bitbucket.org/HARCHHI/drone-test/commits/53d00d9d3a93b3e62989c24145a2df06fc686767))

# [1.7.0](https://bitbucket.org/HARCHHI/drone-test/compare/1.6.6...1.7.0) (2021-08-26)


### Bug Fixes

* change ([3876292](https://bitbucket.org/HARCHHI/drone-test/commits/38762923a0fdabaaea0b2aa8506c0716fe562591))
* fix ([3ba05b4](https://bitbucket.org/HARCHHI/drone-test/commits/3ba05b41ed5c48f8477c84bf6a96913959a6f238))
* fix something ([d411f22](https://bitbucket.org/HARCHHI/drone-test/commits/d411f2207718ea278f863117469fd3a0740b20d5))
* fix something ([ceef89a](https://bitbucket.org/HARCHHI/drone-test/commits/ceef89abcc2739dba76b8e2f3bb380aa2c804ab4))
* **drone:** 嘗試修正publish ref ([6fced2b](https://bitbucket.org/HARCHHI/drone-test/commits/6fced2b1b06f0b252a1b1613d0d917a7b88c3962))
* fix something ([d4fe7b8](https://bitbucket.org/HARCHHI/drone-test/commits/d4fe7b8ee8215edb63a708b5da32f05f6b0f59d3))


### Features

* some feat ([8cc498c](https://bitbucket.org/HARCHHI/drone-test/commits/8cc498c7ec3a71f328dba961063830ae6d086025))

# [1.7.0-beta.3](https://bitbucket.org/HARCHHI/drone-test/compare/1.7.0-beta.2...1.7.0-beta.3) (2021-08-26)


### Bug Fixes

* fix something ([d411f22](https://bitbucket.org/HARCHHI/drone-test/commits/d411f2207718ea278f863117469fd3a0740b20d5))

# [1.7.0-beta.2](https://bitbucket.org/HARCHHI/drone-test/compare/1.7.0-beta.1...1.7.0-beta.2) (2021-08-26)


### Bug Fixes

* fix ([3ba05b4](https://bitbucket.org/HARCHHI/drone-test/commits/3ba05b41ed5c48f8477c84bf6a96913959a6f238))

# [1.7.0-beta.1](https://bitbucket.org/HARCHHI/drone-test/compare/1.6.6...1.7.0-beta.1) (2021-08-26)


### Bug Fixes

* fix something ([ceef89a](https://bitbucket.org/HARCHHI/drone-test/commits/ceef89abcc2739dba76b8e2f3bb380aa2c804ab4))
* **drone:** 嘗試修正publish ref ([6fced2b](https://bitbucket.org/HARCHHI/drone-test/commits/6fced2b1b06f0b252a1b1613d0d917a7b88c3962))
* change ([3876292](https://bitbucket.org/HARCHHI/drone-test/commits/38762923a0fdabaaea0b2aa8506c0716fe562591))
* fix something ([d4fe7b8](https://bitbucket.org/HARCHHI/drone-test/commits/d4fe7b8ee8215edb63a708b5da32f05f6b0f59d3))


### Features

* some feat ([8cc498c](https://bitbucket.org/HARCHHI/drone-test/commits/8cc498c7ec3a71f328dba961063830ae6d086025))

## [1.6.4](https://bitbucket.org/HARCHHI/drone-test/compare/1.6.3...1.6.4) (2021-01-11)


### Bug Fixes

* drone ([1796c8b](https://bitbucket.org/HARCHHI/drone-test/commits/1796c8b4a830b18b587ab9df81d0fa45b6f6bc41))
* ping step ([b6b636b](https://bitbucket.org/HARCHHI/drone-test/commits/b6b636b8c2d2f8611bab0f738cde6abbfcd2146d))
* remove ping ([9a0fe78](https://bitbucket.org/HARCHHI/drone-test/commits/9a0fe78f8c2463a87494ac3ca9a421733cd0e503))

## [1.6.3](https://bitbucket.org/HARCHHI/drone-test/compare/1.6.2...1.6.3) (2021-01-08)


### Bug Fixes

* ping 3 ([1187478](https://bitbucket.org/HARCHHI/drone-test/commits/118747883b35228b4516edfccae3c640855d9e59))

## [1.6.2](https://bitbucket.org/HARCHHI/drone-test/compare/1.6.1...1.6.2) (2021-01-08)


### Bug Fixes

* ping 2 ([8d5555a](https://bitbucket.org/HARCHHI/drone-test/commits/8d5555a4ee51016801b53ff27e3115bcb59127e0))
* try ping ([4d1f2be](https://bitbucket.org/HARCHHI/drone-test/commits/4d1f2bed21f88ce51d6baa4134d90a170a5f3a63))

## [1.6.1](https://bitbucket.org/HARCHHI/drone-test/compare/1.6.0...1.6.1) (2021-01-08)


### Bug Fixes

* puspu ([b7e8231](https://bitbucket.org/HARCHHI/drone-test/commits/b7e823170f42e28f52df640d7911647b1196937b))

# [1.6.0](https://bitbucket.org/HARCHHI/drone-test/compare/1.5.7...1.6.0) (2020-05-27)


### Bug Fixes

* **ci:** fix ([8fadd22](https://bitbucket.org/HARCHHI/drone-test/commits/8fadd22836876874262cc546e4b88d2f0d9972d7))


### Features

* **change:** ch ([30f3aaa](https://bitbucket.org/HARCHHI/drone-test/commits/30f3aaa67f3fdae7378d0ae62e673b1300d0f8c1))

## [1.5.7](https://bitbucket.org/HARCHHI/drone-test/compare/1.5.6...1.5.7) (2020-01-03)


### Bug Fixes

* **drone:** 驗證結束 ([70a73b3](https://bitbucket.org/HARCHHI/drone-test/commits/70a73b392426adbb0d373894fd2ff1ee582970d5))

## [1.5.6](https://bitbucket.org/HARCHHI/drone-test/compare/1.5.5...1.5.6) (2020-01-03)


### Bug Fixes

* **drone:** insecure檢證 ([e1fc71c](https://bitbucket.org/HARCHHI/drone-test/commits/e1fc71c8f3aa569ec6c295e690af23c3a63145d4))

## [1.5.5](https://bitbucket.org/HARCHHI/drone-test/compare/1.5.4...1.5.5) (2020-01-03)


### Bug Fixes

* **drone:** 嘗試修復dns問題 ([dc70c6d](https://bitbucket.org/HARCHHI/drone-test/commits/dc70c6d2a4181b3174fa81375efac25352e3b836))

## [1.5.4](https://bitbucket.org/HARCHHI/drone-test/compare/1.5.3...1.5.4) (2020-01-02)


### Bug Fixes

* **drone:** host to ip ([2339426](https://bitbucket.org/HARCHHI/drone-test/commits/23394264e132d0dddc7e60334c752c23c915d5d7))

## [1.5.3](https://bitbucket.org/HARCHHI/drone-test/compare/1.5.2...1.5.3) (2020-01-02)


### Bug Fixes

* **release:** trigger CD ([f904f46](https://bitbucket.org/HARCHHI/drone-test/commits/f904f46f4381eba9226f3786b3e908264cfef64d))

## [1.5.2](https://bitbucket.org/HARCHHI/drone-test/compare/1.5.1...1.5.2) (2020-01-02)


### Bug Fixes

* **drone:** 修正錯誤的registry位置 ([b271d4c](https://bitbucket.org/HARCHHI/drone-test/commits/b271d4cb50f5efd293c6dbb2b9354cf8cb6ace1b))

## [1.5.1](https://bitbucket.org/HARCHHI/drone-test/compare/1.5.0...1.5.1) (2020-01-02)


### Bug Fixes

* **drone:** 試圖修復push  image問題 ([cd43d21](https://bitbucket.org/HARCHHI/drone-test/commits/cd43d212026d9122d3ab43cb485b943688f2a847))

# [1.2.0](https://bitbucket.org/HARCHHI/drone-test/compare/1.1.1...1.2.0) (2019-12-26)


### Bug Fixes

* **drone:** fix drone.yml image error ([7815494](https://bitbucket.org/HARCHHI/drone-test/commits/78154940929817ee61b35c39d01d29abc2f40aad))
* **drone:** 修正drone使用的node image ([940f38c](https://bitbucket.org/HARCHHI/drone-test/commits/940f38c61190001824770e0a2f1956b63e236aa1))


### Features

* **drone:** ready for drone ([b9ffd8d](https://bitbucket.org/HARCHHI/drone-test/commits/b9ffd8d6ffada62408a3c444dfc5cd90041be713))

## [1.1.1](https://bitbucket.org/HARCHHI/drone-test/compare/1.1.0...1.1.1) (2019-12-25)


### Bug Fixes

* **change:** fix some thing ([125fd45](https://bitbucket.org/HARCHHI/drone-test/commits/125fd457e41de7cce6850457e34ea225f030980f))

# [1.1.0](https://bitbucket.org/HARCHHI/drone-test/compare/1.0.0...1.1.0) (2019-12-25)


### Features

* **change:** make some change ([7b21437](https://bitbucket.org/HARCHHI/drone-test/commits/7b2143715300759dd437864d390be44c55b35bea))

# 1.0.0 (2019-12-25)


### Bug Fixes

* node image name wrong ([fd795a2](https://bitbucket.org/HARCHHI/drone-test/commits/fd795a2b659398592a6c0c0f59e577b280ed4b72))


### Code Refactoring

* **folder:** 進行引入semantic-release的調整 ([67b99bf](https://bitbucket.org/HARCHHI/drone-test/commits/67b99bfe8a7066af9b8441cbf4491f866c13d755))


### Features

* change ([4ad9ef5](https://bitbucket.org/HARCHHI/drone-test/commits/4ad9ef5033ccc9e5ce73b172929348dfca9199b8))


### BREAKING CHANGES

* **folder:** remove project1, project2
