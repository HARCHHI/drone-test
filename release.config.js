const branch = process.env.DRONE_BRANCH;

const config = {
  plugins: [
    '@semantic-release/release-notes-generator',
    ['@semantic-release/npm', { "npmPublish": false }],
    ['@semantic-release/git', {
      "message": "chore(release): ${nextRelease.version} [ci skip]\n\n${nextRelease.notes}"
    }]
  ],
  "branches": [
    { "name": "master" },
    { "name": "release\\\/*", "channel": "release", "prerelease": "beta" }
  ],
  "repositoryUrl": "git@bitbucket.org:HARCHHI/drone-test.git",
  "tagFormat": "${version}"
}

if (branch === 'master') config.plugins = ['@semantic-release/changelog', ...config.plugins];

module.exports = config;
